---
date: 19 Dec 2021
title: "Environnement I: Codes-barres, iBeacons et NFC"
subtitle: Laboratoire 3
author:
  - Roosemberth Palacios
  - Joan Maillard
---

# Introduction

The goal of this lab is to familiarize ourselves with the implementation of
several concepts: iBeacons, NFC tags and QR codes.
These are ubiquitous technologies used in the contexts of location, low-capacity
data transfer from a static source, two-factor authentication or others
short-range communication applications.

## N.B. for iBeacon section:

Unfortunately, even after following several online guides, official or otherwise,
as well as trying several fixes of our own, we were unable to make the ListView
display anything, be it dynamically generated values or even static defaults,
except a red background. The results of the iBeacon scans can be found in the
application's logs instead.

## N.B. for NFC section:

User and password for authentication are `test` and `test`.

# Answers to questions

## 2.4.1

No: The Android API does not provide a cryptographically-secure
identintification process for tags.
Any of the elements provided by the Android API could be copied into a blank NFC
tag or emulated by a sofisticated attacker.
Moreover, the MIFARE API does provide a set of cryptographic operations that can
be used to attest the identity of a tag, but history has shown many security
issues with MIFARE's implementation: See
https://fahrplan.events.ccc.de/congress/2007/Fahrplan/events/2378.en.html

## 2.4.2

A solution based on the presence of an iBeacon presents the same issue as that
of the cloning of an NFC tag, if not worse. The presence of a serial number on
the NFC tag, akin to the presence of a service UUID on an iBeacon, is equally
easy to spoof and/or verify. The only real difference between the two is range,
regarding which the iBeacon is at a loss because it is possible to clone it
from a much longer range.

A similar technology could be used to broadcast a nonce with a limited duration
instead, but this is still an issue since a sofisticated attacker could install
a receiver to catch the current nonce and transmit it (e.g. over the internet)
to a different location.

## 3.2.1

The maximum amount of data that can be stored in a QR code depends on its
particular format.
Several of them exist; however, the standard typically designated by the
"QR-code" name can hold up to 7089 numeric characters, or 4296 alphanumeric
characters, or 2953 bytes.
It depends on the version used, as well as the level of error correction.

It therefore stands to reason that holding over 500 characters of text (while
remaining within a similar order of magnitude, of course), a vCard (assuming no
image is present) or a Covid certificate, is perfectly reasonable - and even
conforms to the preferred method of presentation used by the latter example.

## 3.2.2

Dynamic QR codes basically contain a URI the code reader can recognize and
follow with a browser.
This format of access to information is different to static codes in that
instead of storing the information transmitted to the user directly, a dynamic
QR code stores an address, oftentimes a webpage, that can be opened by the user
relatively seamlessly in order to access that information.

This has the advantage of permitting the transmission of more data than the
QR-code itself can hold, as well as the possibility of giving the user a quick
way to access a webpage or application, for example, in a way that is very easy
to make available to many users at once, and without the need to refresh it as
often as the distributed data of interest changes.

## 4.2

It seems interesting and relevant to preface the answer to this question by
mentioning the fact that not all Apple mobile devices feature NFC compatibility.
It therefore sounds reasonable that Apple would develop a type of device that
piggy-backs on a pre-existing technology to implement a reasonably similar
system, which _in fine_ took the shape of iBeacons.

However, the technology that iBeacons piggy-back on intrinsiqually require the
usage of active terminals on both ends.
In essence, the entire fully-passive use-case that NFC tags can be used for is
lost: there is no way to induce the current an iBeacon needs to operate through
an RF field.
This difference prohibits the usage of iBeacons in the wild without maintenance,
whereas NFC tags can lay dormant as long as they remain unused and only activate
reactively as a user's device scans them.

iBeacons' most commonly encountered use case nowadays is Twint beacons.
They use, if not that technology specifically, at least a very similar type of
device that allows the application, when it's running, to identify its location,
and which cash register to query in order to present the user with the correct
payment information.
Given the privileged position of this type of device, it is entirely conceivable
that the aim of such a service as Twint beacons was implemented in the hopes of
being compatible with as many devices as possible; therefore, it was not an
option to use NFC tags in this case (see first paragraph as an example to why).

On the other hand, information such as bus schedule tags are not so easy to
maintain out in the wild.
The MBC, for example, exclusively makes use of NFC tags as opposed to iBeacons
in order to redirect the user towards the proper webpage or application that
would contain such information. The usage of iBeacons would require some sort of
energy source, and therefore the deployment of much heavier-to-maintain an
infrastructure, either in the shape of a battery replacement schedule, or a
recharge system, such as solar panels or a connexion to the electrical network,
all of which is far from as handy as just dropping a tag inside a frame,
sticking it to a lighting pole or a bench and leaving it at that.

In the light of all those elements, it stands to reason that iBeacons can be a
replacement technology for NFC in certain use cases, and vice and versa;
however, the reality of things suggests that the differences between them
radically complicate the usage of one in certain conditions, but not the other,
and vice versa.

Therefore, our conclusion is that iBeacons and NFC are not fully interchangeable
in their usage; instead, each presents a range of use cases that sometimes
overlap, be it through the wide variations of hardware between mobile devices,
or simply environmental conditions.
