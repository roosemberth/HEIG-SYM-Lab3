package ch.heigvd.sym.lab3

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import ch.heigvd.sym.lab3.databinding.ActivityIbeaconScannerBinding
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.BeaconParser
import org.altbeacon.beacon.Region

private const val PERM_REQ_BLE_PERMISSIONS: Int = 101
private const val IBEACON_TICKET: String = "region-id"

class IBeaconScannerActivity : AppCompatActivity() {
    private lateinit var binding: ActivityIbeaconScannerBinding
    private lateinit var scanner: BeaconParser
    private lateinit var beaconManager: BeaconManager
    private lateinit var region: Region
    private val beaconsPropertiesList = mutableListOf("I", "am", "a", "stegosaurus")
    private lateinit var myAdapter:ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ibeacon_scanner)
        binding = ActivityIbeaconScannerBinding.inflate(layoutInflater)
        myAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, beaconsPropertiesList)
        myAdapter.setNotifyOnChange(true)
        binding.vBeaconsListDisplay.adapter = myAdapter
        Log.d(TAG, binding.vBeaconsListDisplay.adapter.toString())
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestBlePermissions()
        } else {
            initBleScanner()
        }

    }

    private val beaconsObserver = Observer<Collection<Beacon>> {beacons ->
        myAdapter.clear()
        var temp = mutableListOf<String>()
        for (beacon: Beacon in beacons) {
            Log.d(TAG, "found beacon at BT address ${beacon.bluetoothAddress}")
            temp.add("UUID: " + beacon.serviceUuid + ", RSSI: " + beacon.rssi + ", Major: " + beacon.id2 + ", Minor: " + beacon.id3)
            // generate list of things to display, then replace current display by new data
        }
        runOnUiThread(object: Runnable {
            override fun run() {
                myAdapter.addAll(temp)
                Log.d(TAG, beaconsPropertiesList.toString())
                myAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun initBleScanner() {
        beaconManager = BeaconManager.getInstanceForApplication(this)
        scanner = BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")
        beaconManager.getBeaconParsers().clear()
        beaconManager.getBeaconParsers().add(scanner)
        beaconManager.setEnableScheduledScanJobs(false)
        beaconManager.setBackgroundBetweenScanPeriod(5000)
        beaconManager.setBackgroundScanPeriod(1100)
        region = Region(IBEACON_TICKET, null, null, null)
        beaconManager.startMonitoring(region)
        beaconManager.startRangingBeacons(region)
        val regionViewModel = BeaconManager.getInstanceForApplication(this).getRegionViewModel(region)
        regionViewModel.rangedBeacons.observeForever(beaconsObserver)
    }

    override fun onStop() {
        super.onStop()
        beaconManager.stopMonitoring(region)
        beaconManager.stopRangingBeacons(region)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERM_REQ_BLE_PERMISSIONS -> {
                val permissionWasGranted = permissions.zip(grantResults.asIterable())
                    .any { (perm, permStatus) -> Manifest.permission.ACCESS_FINE_LOCATION == perm && PackageManager.PERMISSION_GRANTED == permStatus }
                if (!permissionWasGranted) {
                    requestBlePermissions()
                } else {
                    initBleScanner()
                }
            }
            else -> {
                Log.e(TAG, "OnRequestPermissionsResult: Unknown request code.")
            }
        }
    }

    private fun requestBlePermissions() {
        AlertDialog.Builder(this)
            .setTitle("Bluetooth and position permissions required.")
            .setCancelable(true)
            .setOnCancelListener { finish() }
            .setMessage("In order to scan for iBeacons, we need to request access to the position and Bluetooth on your device.")
            .setPositiveButton("Proceed") { _, _ ->
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERM_REQ_BLE_PERMISSIONS
                )
            }
            .setNegativeButton("Cancel") { _, _ ->
                Log.i(
                    "BlePermRequestDialog",
                    "Closing parent activity since user cancelled the action."
                )
                finish()
            }
            .show()
    }

    companion object {
        val TAG = "IBeaconScannerActivity"
    }

}