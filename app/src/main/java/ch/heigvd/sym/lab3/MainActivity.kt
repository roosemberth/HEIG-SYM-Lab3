package ch.heigvd.sym.lab3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ch.heigvd.sym.lab3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnOpenBarcodeView.setOnClickListener {
            startActivity(Intent(this, BarcodeScannerActivity::class.java))
        }
        binding.btnOpeniBeaconView.setOnClickListener {
            startActivity(Intent(this, IBeaconScannerActivity::class.java))
        }
        binding.btnOpenNfcView.setOnClickListener {
            startActivity(Intent(this, NfcTagScannerActivity::class.java))
        }
    }
}
