package ch.heigvd.sym.lab3

import android.os.CountDownTimer
import java.util.function.Consumer

/**
 * Manages the security clearance level policy
 */
object SecurityClearanceService {
    enum class ClearanceLevel {
        High, Med, Low, None
    }

    var clearanceLevel: ClearanceLevel = ClearanceLevel.None
        private set

    private val timerPeriodMs: Long = 10000 // 10s

    private var tickSubscribers: MutableList<Consumer<Long?>> = mutableListOf()

    private fun mkCountdownTimer(): CountDownTimer {
        return object : CountDownTimer(timerPeriodMs, 1000) {
            override fun onTick(remaining: Long) {
                tickSubscribers.forEach { s -> s.accept(remaining) }
            }

            override fun onFinish() {
                downgrade();
                val subscribers = tickSubscribers.toList()
                tickSubscribers.clear()
                subscribers.forEach { s -> s.accept(null) }
            }
        }
    }

    /**
     * Subscribes to a timer that periodically provides information on the remaining time
     * until the security level downgrades.
     * The subscriber is called with null when the timer has expired and the security level
     * has downgraded.
     *
     * Returns the period downgrades occur in ms, irrespective of the elapsed time.
     */
    fun subscribeToExpirationTick(consumer: Consumer<Long?>): Long {
        tickSubscribers.add(consumer)
        return timerPeriodMs;
    }

    /**
     * Downgrades the current security clearance level by one step.
     * See [ClearanceLevel]
     */
    fun downgrade() {
        when (clearanceLevel) {
            ClearanceLevel.None -> {}
            ClearanceLevel.Low -> clearanceLevel = ClearanceLevel.None
            ClearanceLevel.Med -> clearanceLevel = ClearanceLevel.Low
            ClearanceLevel.High -> clearanceLevel = ClearanceLevel.Med
        }
        if (clearanceLevel != ClearanceLevel.None)
            mkCountdownTimer().start()
    }

    /**
     * Sets the clearance level to the highest value.
     * See [ClearanceLevel]
     */
    fun upgradeToHighest() {
        clearanceLevel = ClearanceLevel.High
        mkCountdownTimer().start()
    }
}