package ch.heigvd.sym.lab3

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import ch.heigvd.sym.lab3.databinding.ActivityBarcodeScannerBinding
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.DefaultDecoderFactory

private const val PERM_REQ_CAMERA_PERMISSIONS: Int = 100

class BarcodeScannerActivity : AppCompatActivity(),
    BarcodeResultFragment.BarcodeResultFragmentListener {
    private lateinit var binding: ActivityBarcodeScannerBinding

    private var lastText: String? = null
    private var keepScanning: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBarcodeScannerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermissions()
        } else {
            initBarcodeScanner()
        }
    }

    private fun initBarcodeScanner() {
        val formats = listOf(BarcodeFormat.CODE_39, BarcodeFormat.QR_CODE)
        binding.barcodeScanner.also {
            it.barcodeView.decoderFactory = DefaultDecoderFactory(formats)
            it.initializeFromIntent(intent)
            it.decodeContinuous { result ->
                if (result.text == null)
                    return@decodeContinuous;
                if (result.text.equals(lastText)) {
                    lastText = null // Simple filter to avoid duplicated scan results.
                    return@decodeContinuous;
                }

                lastText = result.text
                binding.barcodeScanner.setStatusText(result.text)
                binding.barcodeScanner.pause()
                BarcodeResultFragment.newInstance(result.text, result.bitmap, keepScanning)
                    .show(supportFragmentManager, "Barcode result dialog")
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERM_REQ_CAMERA_PERMISSIONS -> {
                val permissionWasGranted = permissions.zip(grantResults.asIterable())
                    .any { (perm, permStatus) -> Manifest.permission.CAMERA == perm && PackageManager.PERMISSION_GRANTED == permStatus }
                if (!permissionWasGranted) {
                    requestCameraPermissions()
                } else {
                    initBarcodeScanner()
                }
            }
            else -> {
                Log.e("BarcodeScannerActivity", "OnRequestPermissionsResult: Unknown request code.")
            }
        }
    }

    private fun requestCameraPermissions() {
        AlertDialog.Builder(this)
            .setTitle("Camera permissions required.")
            .setCancelable(true)
            .setOnCancelListener { finish() }
            .setMessage("In order to scan QR codes, we need to request access to the camera on your device.")
            .setPositiveButton("Proceed") { _, _ ->
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    PERM_REQ_CAMERA_PERMISSIONS
                );
            }
            .setNegativeButton("Cancel") { _, _ ->
                Log.i(
                    "CameraPermRequestDialog",
                    "Closing parent activity since user cancelled the action."
                )
                finish()
            }
            .show()
    }

    override fun onPause() {
        super.onPause()
        binding.barcodeScanner.pause()
    }

    override fun onResume() {
        super.onResume()
        binding.barcodeScanner.resume()
    }

    override fun onFragmentCloseShouldKeepScanning(shouldKeepScanning: Boolean) {
        keepScanning = shouldKeepScanning
        if (!keepScanning) {
            finish()
        } else {
            binding.barcodeScanner.resume()
        }
    }
}