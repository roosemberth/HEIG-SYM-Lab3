package ch.heigvd.sym.lab3

import java.security.MessageDigest
import java.security.SecureRandom

/**
 * Stores a user credential.
 *
 * Such credential can be obtained from a username and password and used for later authentication.
 *
 * Two credentials obtained from the same username/password combination are not equal.
 * To authenticate a password use the [authenticate] method on the instance.
 *
 * Implementation note: We use Lists of Bytes as opposed to ByteArrays because of their equality implementation.
 */
data class SimpleUserCredential(val user: String, private val salt: List<Byte>, private val hashed: List<Byte>) {
    /**
     * Verifies whether the specified username and password matches this credential.
     */
    fun authenticate(username: String, password: String): Boolean {
        return this == SimpleUserCredential(username, salt, hashUtf8(salt, password))
    }

    companion object {
        fun fromUsernameAndPassword(user: String, pass: String): SimpleUserCredential {
            // TODO(@Roos): This should be audited.
            val salt = SecureRandom.getInstanceStrong().generateSeed(32).toList()
            return SimpleUserCredential(user, salt, hashUtf8(salt, pass))
        }

        private fun hashUtf8(salt: List<Byte>, data: String): List<Byte> {
            return hash(salt, data.encodeToByteArray().toList())
        }

        private fun hash(salt: List<Byte>, data: List<Byte>): List<Byte> {
            val md = MessageDigest.getInstance("SHA-384")
            md.update(salt.toByteArray())
            return md.digest(data.toByteArray()).toList()
        }
    }
}