package ch.heigvd.sym.lab3

import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import ch.heigvd.sym.lab3.databinding.FragmentBarcodeResultBinding
import java.io.ByteArrayOutputStream


private const val ARG_RES_TEXT = "Scan result Text"
private const val ARG_RES_IMG = "Scan result Image"
private const val ARG_RES_IMG_LEN = "Scan result Image length"
private const val ARG_KEEP_SCANNING_CHECKBOX_DEFAULT = "Keep scanning checkbox default value"

/**
 * A fragment displaying the barcode scan result to the user.
 *
 * Activities using this fragment SHOULD implement the [BarcodeResultFragmentListener] interface
 * to receive the user preference on whether the application should continue to scan barcodes.
 *
 * Use the [BarcodeResultFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BarcodeResultFragment : DialogFragment() {
    private lateinit var resultText: String
    private lateinit var resultImg: Bitmap
    private var keepScanningCheckboxDefault: Boolean = false
    private lateinit var binding: FragmentBarcodeResultBinding
    private var onDoneListener: BarcodeResultFragmentListener? = null
    private lateinit var uri: Uri

    interface BarcodeResultFragmentListener {
        /**
         * Called upon closing the fragment with a user preference whether the application should keep scanning barcodes.
         */
        fun onFragmentCloseShouldKeepScanning(shouldKeepScanning: Boolean)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = FragmentBarcodeResultBinding.inflate(LayoutInflater.from(context))
        arguments?.let {
            resultText = it.getString(ARG_RES_TEXT)!!
            resultImg = BitmapFactory.decodeByteArray(it.getByteArray(ARG_RES_IMG), 0, it.getInt(ARG_RES_IMG_LEN))
            keepScanningCheckboxDefault = it.getBoolean(ARG_KEEP_SCANNING_CHECKBOX_DEFAULT)
            // TODO(@Roos): What if text is not an URI?
            uri = Uri.parse(resultText)
        }

        val builder = AlertDialog.Builder(context)
        builder.setView(binding.root)

        binding.barcodeResultImg.setImageBitmap(resultImg)
        binding.barcodeResultText.text = resultText
        binding.keepScannerActiveCheckbox.isChecked = keepScanningCheckboxDefault

        binding.buttonCopy.setOnClickListener {
            val clipboard: ClipboardManager? = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
            clipboard?.setPrimaryClip(ClipData.newPlainText("Barcode value", resultText))
            Toast.makeText(context, "Copied to clipboard!", Toast.LENGTH_SHORT).show()
            dismiss()
        }
        binding.buttonOpen.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, uri))
            dismiss()
        }
        binding.buttonShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, resultText)
            startActivity(Intent.createChooser(intent, "Share via"))
            dismiss()
        }

        return builder.create()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            onDoneListener = context as BarcodeResultFragmentListener
        } catch (e: ClassCastException) {
            Log.e("BarcodeResultFragment", "Parent is not a BarcodeResultFragmentListener. User choice will be discarded.")
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDoneListener?.onFragmentCloseShouldKeepScanning(binding.keepScannerActiveCheckbox.isChecked)
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        // Action was canceled, is it expected that the user can continue scanning.
        binding.keepScannerActiveCheckbox.isChecked = true
        // Processing continues on the onDismiss method.
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment BarcodeResultFragment.
         */
        @JvmStatic
        fun newInstance(resultText: String, resultImg: Bitmap, keepScanningCheckboxDefault: Boolean) =
            BarcodeResultFragment().apply {
                arguments = Bundle().apply {
                    val stream = ByteArrayOutputStream()
                    resultImg.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    val imgBytes = stream.toByteArray()

                    putString(ARG_RES_TEXT, resultText)
                    putByteArray(ARG_RES_IMG, imgBytes)
                    putInt(ARG_RES_IMG_LEN, imgBytes.size)
                    putBoolean(ARG_KEEP_SCANNING_CHECKBOX_DEFAULT, keepScanningCheckboxDefault)
                }
            }
    }
}
