package ch.heigvd.sym.lab3

object UserService {
    val defaultUser = SimpleUserCredential.fromUsernameAndPassword("test", "test")
    var currentUser: SimpleUserCredential? = null
}