package ch.heigvd.sym.lab3

import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import ch.heigvd.sym.lab3.databinding.ActivityNfcTagScannerBinding
import android.nfc.NdefRecord
import java.io.UnsupportedEncodingException
import kotlin.experimental.and


class NfcTagScannerActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNfcTagScannerBinding
    private lateinit var nfcAdapter: NfcAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNfcTagScannerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.authenticateButton.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        val mNfcAdapter = NfcAdapter.getDefaultAdapter(this)
        if (mNfcAdapter == null || !mNfcAdapter.isEnabled) {
            AlertDialog.Builder(this)
                .setTitle("NFC adapter required.")
                .setCancelable(false)
                .setOnCancelListener { finish() }
                .setMessage("The application was unable to find a suitable NFC adapter. Please make sure NFC is available and enabled in your system settings.")
                .setPositiveButton("Go back") { _, _ ->
                    Log.i(
                        "NfcInitDialog",
                        "Closing activity since could not obtain NFC."
                    )
                    finish()
                }
                .show()
            return
        }
        nfcAdapter = mNfcAdapter
        handleIntent(intent)
    }

    override fun onResume() {
        super.onResume()
        if (UserService.currentUser == null) {
            binding.authStatusTextView.text = "Not authenticated."
            binding.authenticateButton.visibility = View.VISIBLE
            binding.clearanceHintTextView.visibility = View.GONE
        } else {
            binding.authStatusTextView.text = "Authenticated."
            binding.authenticateButton.visibility = View.GONE
            binding.clearanceStatusTextView.text = "Waiting for security badge"
            binding.clearanceHintTextView.visibility = View.VISIBLE
            setupNfcDispatch()
        }
    }

    override fun onPause() {
        super.onPause()
        stopNfcDispatch()
    }

    fun setupNfcDispatch() {
        intent = Intent(this, this::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val filter = IntentFilter()
        filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED)
        filter.addCategory(Intent.CATEGORY_DEFAULT)
        filter.addDataType("text/plain")
        Log.i("NfcActivity", "Registering NFC dispatch")
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, arrayOf(filter), emptyArray())
    }

    fun stopNfcDispatch() {
        Log.i("NfcActivity", "Deregistering NFC dispatch")
        nfcAdapter.disableForegroundDispatch(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent?) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED == intent?.action) {
            Thread {
                val tag: Tag? = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
                val ndef = Ndef.get(tag)
                if (ndef == null) {
                    runOnUiThread {
                        Toast.makeText(application, "Tag is not valid.", Toast.LENGTH_SHORT).show()
                    }
                    return@Thread
                }
                val records = ndef.cachedNdefMessage.records.map { r -> readText(r) }.toList()
                if (records.size != 4) {
                    runOnUiThread {
                        android.widget.Toast.makeText(
                            application,
                            "Tag is not valid.",
                            android.widget.Toast.LENGTH_SHORT
                        ).show()
                    }
                    Log.i("NFC Auth", "NFC tag does not match the expected length.")
                    return@Thread
                }
                if ("test" != records[0]) {
                    runOnUiThread {
                        android.widget.Toast.makeText(
                            application,
                            "Tag is not valid.",
                            android.widget.Toast.LENGTH_SHORT
                        ).show()
                    }
                    Log.i("NFC Auth", "NFC tag does not match the expected contents.")
                    return@Thread
                }
                runOnUiThread {
                    SecurityClearanceService.upgradeToHighest()
                    watchClearance()
                }
            }.start()
        }
    }

    @Throws(UnsupportedEncodingException::class)
    private fun readText(record: NdefRecord): String? {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */
        val payload = record.payload
        val textEncoding =
            if (payload[0] and (128).toByte() == (0).toByte()) Charsets.UTF_8 else Charsets.UTF_16
        val languageCodeLength: Int = (payload[0] and (51).toByte()).toInt()

        return String(
            payload,
            languageCodeLength + 1,
            payload.size - languageCodeLength - 1,
            textEncoding
        )
    }

    private fun watchClearance() {
        binding.progressBar.visibility = View.VISIBLE
        binding.clearanceStatusTextView.text =
            SecurityClearanceService.clearanceLevel.toString()
        binding.progressBar.isIndeterminate = false
        binding.progressBar.progress = 0
        binding.progressBar.max = SecurityClearanceService.subscribeToExpirationTick { remaining ->
            if (remaining != null) {
                remaining.also { binding.progressBar.progress = it.toInt() }
            } else {
                val newClearance = SecurityClearanceService.clearanceLevel
                if (newClearance != SecurityClearanceService.ClearanceLevel.None) {
                    binding.clearanceStatusTextView.text = newClearance.toString()
                    watchClearance()
                } else {
                    binding.clearanceStatusTextView.text = "Waiting for security badge"
                    binding.progressBar.visibility = View.GONE
                }
            }
        }.toInt()
    }
}