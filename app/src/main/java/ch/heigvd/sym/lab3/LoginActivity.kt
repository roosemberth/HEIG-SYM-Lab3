package ch.heigvd.sym.lab3

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import ch.heigvd.sym.lab3.databinding.ActivityLoginBinding

class LoginActivity : Activity() {
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.loginButton.setOnClickListener {
            val user = binding.username.text.toString()
            val pass = binding.password.text.toString()
            if (UserService.defaultUser.authenticate(user, pass)) {
                UserService.currentUser = UserService.defaultUser
                finish()
            } else {
                Toast.makeText(this, "Unacceptable credentials", Toast.LENGTH_SHORT)
            }
        }
    }
}