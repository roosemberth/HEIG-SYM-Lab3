package ch.heigvd.sym.lab3

import junit.framework.TestCase
import org.junit.Test

class SimpleUserCredentialTest : TestCase() {
    @Test
    fun testCanAuthenticateWithSameCredentials() {
        val user: String = "Test user"
        val pass: String = "Test password"
        assertTrue(
            SimpleUserCredential.fromUsernameAndPassword(user, pass).authenticate(user, pass)
        )
    }

    @Test
    fun testCailsToAuthDifferentCreds() {
        val user: String = "Test user"
        val pass: String = "Test password"
        val badPass: String = "Test password, but incorrect"
        assertFalse(
            SimpleUserCredential.fromUsernameAndPassword(user, pass).authenticate(user, badPass)
        )
    }

    @Test
    fun testCredsNotEqualWithSameData() {
        val user: String = "Test user"
        val pass: String = "Test password"
        assertNotSame(
            SimpleUserCredential.fromUsernameAndPassword(user, pass),
            SimpleUserCredential.fromUsernameAndPassword(user, pass)
        )
    }
}
